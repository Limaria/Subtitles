﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using System.Text;


namespace Subtitles
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] file = File.ReadAllLines("text.txt");
            foreach (var f in file)
            {
                Subtitles subtitles = new Subtitles(f);
                Subtitles.ConsoleProduct(subtitles);
                Console.ReadKey();
            }
        }

        class FileReader
        {
            public string time;
            public string instructions;
            public string sentenses;
            public  FileReader(string values)
            {
                if (values.Contains('['))
                {
                    string[] part = values.Split('[', ']');
                    time = part[0];
                    instructions = part[1];
                    sentenses = part[2];
                }
                else
                {
                    time = values.Substring(values.IndexOf(values, 0, 12));
                    instructions = null;
                    sentenses = values.Substring(values.IndexOf(values, 14, values.Length));
                }
            }
        }

        class Subtitles : FileReader
        {
            long start;
            long stop;

            string position;
            string color;

            public Subtitles(string values) : base(values)
            {
                string[] time = this.time.Split('-');
                long[] seconds = new long[2];
                for (int i = 0; i < time.Length; i++)
                {
                    string[] partTime = time[i].Split(':');
                    for (int j = 0; j < partTime.Length; j++) partTime[j] = partTime[j].Trim(' ');

                    if (partTime[0] != "00")
                    {
                        seconds[i] = long.Parse(partTime[0].Trim('0')) * 60 * 1000 + long.Parse(partTime[1].Trim('0')) * 1000;
                    }
                    else
                    {
                        seconds[i] = long.Parse(partTime[1].Trim('0')) * 1000;
                    }
                }

                start = seconds[0];
                stop = seconds[1];

                if (instructions != null)
                {
                    position = instructions.Split(',')[0];
                    color = instructions.Split(',')[1];
                    color = color.Trim(' ');
                }
                else
                {
                    position = "Bottom";
                    color = "White";
                }
            }

            public void ColorConverter()
            {
                switch (color)
                {
                    case "Red":
                        Console.ForegroundColor = ConsoleColor.Red;
                        break;
                    case "Green":
                        Console.ForegroundColor = ConsoleColor.Green;
                        break;
                    case "Yellow":
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        break;
                    case "Blue":
                        Console.ForegroundColor = ConsoleColor.Blue;
                        break;
                    case "White":
                        Console.ForegroundColor = ConsoleColor.White;
                        break;
                }
            }

            public void CursorPosition()
            {
                switch (position)
                {
                    case "Top":
                        Console.SetCursorPosition((Console.WindowWidth / 2) - (sentenses.Length / 2), 0);
                        break;
                    case "Bottom":
                        Console.SetCursorPosition((Console.WindowWidth / 2) - (sentenses.Length / 2), Console.WindowHeight - 1);
                        break;
                    case "Right":
                        Console.SetCursorPosition(0, (Console.WindowHeight / 2) - 1);
                        break;
                    case "Left":
                        Console.SetCursorPosition(Console.WindowWidth - sentenses.Length, (Console.WindowHeight / 2) - 1);
                        break;
                }
            }

            public void Time()
            {
                Console.WriteLine(sentenses);
            }

            public static void ConsoleProduct(Subtitles subtitles)
            {
                subtitles.ColorConverter();
                subtitles.CursorPosition();
                subtitles.Time();
               // long interval = subtitles.stop - subtitles.start;
               // TimerCallback timeCB = new TimerCallback(subtitles.Time);

               // Timer time = new Timer(timeCB, null, 0, interval);
            }
        }
    }
}
